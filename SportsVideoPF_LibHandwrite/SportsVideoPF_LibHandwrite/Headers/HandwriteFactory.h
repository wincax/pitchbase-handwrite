//
//  HandwriteFactory.h
//  SportsVideoPF_LibHandwrite
//  All Rights Reserved, Copyright(c) FUJITSU LIMITED 2016
//

#ifndef HandwriteFactory_h
#define HandwriteFactory_h

#import <Foundation/Foundation.h>
#import "HandwriteProtocol.h"

@interface HandwriteFactory : NSObject
{
    
}

- (id<HandwriteProtocol>)createHandwrite;

@end
#endif /* HandwriteFactory_h */
