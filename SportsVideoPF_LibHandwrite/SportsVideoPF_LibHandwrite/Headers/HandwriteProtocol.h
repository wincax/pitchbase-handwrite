//
//  HandwriteProtocol.h
//  SportsVideoPF_LibHandwrite
//  All Rights Reserved, Copyright(c) FUJITSU LIMITED 2016
//

#ifndef HandwriteProtocol_h
#define HandwriteProtocol_h

@protocol ComparisonViewProtocol <NSObject>

- (void)playControl:(int)play :(float)time;
- (UIView *)setHandwritePlayView:(int)target;

@end

@protocol HandwriteProtocol <NSObject>

- (BOOL)setInstance:(id<ComparisonViewProtocol>)instance;
- (BOOL)initialize;
- (BOOL)resetHandwritePlay:(int)target;
- (BOOL)setHandwritePlayView:(UIView *)playView :(UIImageView *)playImgView :(int)target;
- (BOOL)setFrameRate:(int)fps;
- (BOOL)setColor:(int)color :(int)target;
- (BOOL)setLineWidth:(int)width :(int)target;
- (void)setSlowPlay:(BOOL)slow :(BOOL)play;
- (BOOL)handwriteBegin:(CGPoint)point :(int)target;
//- (void)drawLine:(UIView *)view :(UIImageView *)imgView :(CGPoint)point :(int)target;
- (UIImage *)drawLine:(UIView *)view :(CGPoint)point :(int)target;
//- (void)drawCircleObject:(UIView *)view :(UIImageView *)imgView :(CGPoint)point :(int)target;
- (UIImage *)drawCircleObject:(UIView *)view :(CGPoint)point :(int)target;
//- (void)drawSquareObject:(UIView *)view :(UIImageView *)imgView :(CGPoint)point :(int)target;
- (UIImage *)drawSquareObject:(UIView *)view :(CGPoint)point :(int)target;
- (BOOL)handeraseBegin:(CGPoint)point :(int)target;
//- (void)eraseLine:(UIView *)view :(UIImageView *)imgView :(CGPoint)point :(int)target;
- (UIImage *)eraseLine:(UIView *)view :(CGPoint)point :(int)target;
- (BOOL)endHandwrite :(int)target;
- (BOOL)readHandwriteInfoFile:(NSString *)path :(int)target;
- (BOOL)startDrawing:(int)target;
- (BOOL)stopDrawing:(int)target;
- (int)getThumbnail:(UIView *)view :(int)target :(NSString *)filePath;
- (BOOL)finalize:(int)target;
- (int)handwriteInfoUpload:(NSString *)textFilePath :(NSString *)thumbnailFilePath :(NSString *)play_list_id :(double)timestamp :(NSString *)backendURL :(NSString *)token;
- (BOOL)handwriteInfoDownload:(NSString *)backendURL :(NSString *)token :(NSString *)handwrite_id;
- (int)handwriteReadInfo:(NSString *)dir :(NSString **)textFilePath :(NSString **)thumbnailFilePath :(NSString **)play_list_id :(double *)timestamp :(double *)createTime;

@end



#endif /* HandwriteProtocol_h */
