//
//  Handwrite.m
//  SportsVideoPF_LibHandwrite
//  All Rights Reserved, Copyright(c) FUJITSU LIMITED 2016
//

#import "Handwrite.h"

@implementation HandwriteLib

/*!
 * @brief   インスタンスを設定
 *
 * @param   instance 比較プレーヤのインスタンス
 * @return  NO 引数エラー
 * @note    公開関数。比較プレーヤのインスタンスを設定。
 */
- (BOOL)setInstance:(id<ComparisonViewProtocol>)instance
{
    if (instance == nil) {
        return NO;
    }
    m_instance = instance;
    return YES;
}

/*!
* @brief   初期化
*
* @param   -
* @return  YES 正常終了
* @return  NO ディレクトリ検索失敗
* @note    公開関数
*/
- (BOOL)initialize
{
    BOOL rc;
    // 変数の初期化
    //m_handwriteImg = [[UIImage alloc] init];
    m_receiveStatus = 0;
    m_receivedData = [NSMutableData data];
    m_timerInterval = 30;
    
    // 手書きテキストを削除
    rc = [self cleanHandwriteWithExtension:@[@"txt", @"png"]];
    return rc;
}

/*!
 * @brief   手書き対象viewのリセット
 *
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  YES 正常終了
 * @return  NO 引数エラー
 * @note    公開関数
 */
- (BOOL)resetHandwritePlay:(int)target
{
    if (target < 0) {
        return NO;
    }
    //m_drawCount = 0;
    m_handwritePlayTimer[target] = nil;
    m_handwritePlayImgView[target] = nil;
    m_drawingIndex[target] = 0;
    m_handwriteInfoArray[target] = [NSMutableArray array];
    
    // 初期時の線幅と線色
    m_lineWidth[target] = 4.0f;
    m_lineColor[target] = [UIColor redColor];
    return YES;
}

/*!
 * @brief   手書き表示用viewのセット
 *
 * @param   view UIView
 * @param   imgView UIImageView
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  YES 正常終了
 * @return  NO 引数エラー
 * @note    公開関数。表示用のviewは上位で確保。入力用のviewはセットせず表示用の関数の引数で渡す。
 */
- (BOOL)setHandwritePlayView:(UIView *)view :(UIImageView *)imgView :(int)target
{
    if (target < 0 || view == nil || imgView == nil) {
        return NO;
    }
    // 表示用の変数を初期化
    m_handwritePlayView[target] = view;
    m_handwritePlayImgView[target] = imgView;
    m_bezierPath[target] = [UIBezierPath bezierPath];
    m_bezierPathErase[target] = [UIBezierPath bezierPath];
    
    // スロー再生用変数の初期化
    m_slowPlayTime = 0;
    m_slowStartPlayTime = 0;
    return YES;
}

/*!
 * @brief   描画フレームレートを設定
 *
 * @param   fps フレームレート
 * @return  YES 正常終了
 * @return  NO 引数エラー
 * @note    公開関数
 */
- (BOOL)setFrameRate:(int)fps
{
    if (fps < 1 || fps > 30) {
        return NO;
    }
    float newInterval = 1.0f / fps;
    [self setHandwriteTimerInterval:newInterval];
    return YES;
}

/*!
 * @brief   手書きカラーを設定
 *
 * @param   color 色（1:赤 2:青 3:白）
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  YES 正常終了
 * @return  NO 引数エラー
 * @note    公開関数
 */
- (BOOL)setColor:(int)color :(int)target
{
    if (target < 0) {
        return NO;
    }
    switch (color) {
        case 1:
            m_lineColor[target] = [UIColor redColor];
            break;
            
        case 2:
            m_lineColor[target] = [UIColor blueColor];
            break;
            
        case 3:
            m_lineColor[target] = [UIColor whiteColor];
            break;
            
        default:
            m_lineColor[target] = [UIColor redColor];
            break;
    }
    return YES;
}

/*!
 * @brief   手書き線幅を設定
 *
 * @param   width 幅（1:4 2:5 3:6）
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  YES 正常終了
 * @return  NO 引数エラー
 * @note    公開関数
 */
- (BOOL)setLineWidth:(int)width :(int)target
{
    if (target < 0) {
        return NO;
    }
    switch (width) {
        case 1:
            m_lineWidth[target] = 4.0f;
            break;
            
        case 2:
            m_lineWidth[target] = 5.0f;
            break;
            
        case 3:
            m_lineWidth[target] = 6.0f;
            break;
            
        default:
            m_lineWidth[target] = 4.0f;
            break;
    }
    return YES;
}

/*!
 * @brief   スロー再生かどうかを設定
 *
 * @param   slow スロー再生かどうか（true：スロー再生　false：通常再生）
 * @return  -
 * @note    公開関数
 */
- (void)setSlowPlay:(BOOL)slow :(BOOL)play
{
    m_slowPlay = slow;
    // 再生中であればスロー再生の切り替えを行う
    if (play == YES) {
        if (slow == NO) {
            [m_instance playControl:HANDWRITE_PLAYER_PLAY :0];
        } else {
            [m_instance playControl:HANDWRITE_PLAYER_SLOW_PLAY :0];
        }
    }
}

/*!
 * @brief   描画開始の設定を行う
 *
 * @param   point 描画位置
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  YES 正常終了
 * @return  NO 引数エラー
 * @note    公開関数。タッチ開始時にコールする。
 */
- (BOOL)handwriteBegin:(CGPoint)point :(int)target
{
    if (target < 0) {
        return NO;
    }
    // TODO:色と太さの途中変更は受け付けない(ベジェパスを配列で持てば可能かもしれないが配列を多数持つ必要あり)
    // パスの初期化
    m_bezierPath[target].lineCapStyle = kCGLineCapRound; // 角を角
    //m_bezierPath.lineCapStyle = kCGLineCapButt; // 角を丸
    //m_bezierPath.lineCapStyle = kCGLineCapSquare; // 先が角
    m_bezierPath[target].lineWidth = m_lineWidth[target];
    
    // パスの開始点を設定
    [m_bezierPath[target] moveToPoint:point];
    return YES;
}

/*!
 * @brief   viewにフリーハンド描画する
 *
 * @param   view 描画するview
 * @param   imgView 描画画像
 * @param   point 描画位置
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  UIImage
 * @note    公開関数。タッチ時にコールする。
 */
//- (void)drawLine:(UIView *)view :(UIImageView *)imgView :(CGPoint)point :(int)target
- (UIImage *)drawLine:(UIView *)view :(CGPoint)point :(int)target
{
    if (target < 0 || view == nil) {
        return nil;
    }
    @autoreleasepool {
        // パスの終了点を追加
        [m_bezierPath[target] addLineToPoint:point];
        
        // 非表示の描画領域を生成
        UIGraphicsBeginImageContext(view.frame.size);
        
        // 描画領域に前回までに描画した画像を描画する
        // これだと透明の上に描画できない
        //[m_handwriteImg drawAtPoint:CGPointZero];
        
        // 色をセット
        [m_lineColor[target] setStroke];
        
        // 線を引く
        [m_bezierPath[target] stroke];
        //[m_bezierPathErase strokeWithBlendMode:kCGBlendModeClear alpha:1.0];
        
        // 描画した画像を取得
        UIImage* im = UIGraphicsGetImageFromCurrentImageContext();
        
        // 描画終了
        UIGraphicsEndImageContext();
        
//    imgView.image = nil;
//    [imgView removeFromSuperview];
//    imgView.layer.sublayers = nil;
//    imgView = nil;
//    imgView = [[UIImageView alloc] initWithImage:im];
//    [view addSubview:imgView];
//    [self createThumbnail:imgView: target];
    
        UIImageView* iv = [[UIImageView alloc] initWithImage:im];
        [self createThumbnail:iv: target];
        iv.image = nil;
        iv = nil;

        return im;
    }
}

/*!
 * @brief   viewに円を描画する
 *
 * @param   view 描画するview
 * @param   imgView 描画画像
 * @param   point 描画位置
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  UIImage
 * @note    公開関数。タッチ終了時にコールする。タッチ座標を中心に描画。
 */
//- (void)drawCircleObject:(UIView *)view :(UIImageView *)imgView :(CGPoint)point :(int)target
- (UIImage *)drawCircleObject:(UIView *)view :(CGPoint)point :(int)target
{
    if (target < 0 || view == nil) {
        return nil;
    }
    @autoreleasepool {
        // 円を追加
        m_bezierPath[target] = [UIBezierPath bezierPathWithArcCenter:point radius:30.0f startAngle:0 endAngle:M_PI * 2 clockwise:YES];
        
        // 非表示の描画領域を生成
        UIGraphicsBeginImageContext(view.frame.size);
        
        // 色をセット
        [m_lineColor[target] setStroke];
        
        // 幅をセット
        m_bezierPath[target].lineWidth = m_lineWidth[target];
        
        // 線を引く
        [m_bezierPath[target] stroke];
        
        // 描画した画像を取得
        UIImage* im = UIGraphicsGetImageFromCurrentImageContext();
        
        // 描画終了
        UIGraphicsEndImageContext();
        
//        [imgView removeFromSuperview];
//        imgView.image = nil;
//        imgView = nil;
//        imgView = [[UIImageView alloc] initWithImage:im];
//        [view addSubview:imgView];
//        [self createThumbnail:imgView :target];
    
        UIImageView* iv = [[UIImageView alloc] initWithImage:im];
        [self createThumbnail:iv: target];
        iv.image = nil;
        iv = nil;
        
        return im;
    }
}

/*!
 * @brief   viewに矩形を描画する
 *
 * @param   view 描画するview
 * @param   imgView 描画画像
 * @param   point 描画位置
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  UIImage
 * @note    公開関数。タッチ終了時にコールする。タッチ座標を中心に描画。
 */
//- (void)drawSquareObject:(UIView *)view :(UIImageView *)imgView :(CGPoint)point :(int)target
- (UIImage *)drawSquareObject:(UIView *)view :(CGPoint)point :(int)target
{
    if (target < 0 || view == nil) {
        return nil;
    }
    @autoreleasepool {
        // 円を追加
        m_bezierPath[target] = [UIBezierPath bezierPathWithRect:CGRectMake(point.x - 70 / 2, point.y - 70 / 2, 70, 70)];
        
        // 非表示の描画領域を生成
        UIGraphicsBeginImageContext(view.frame.size);
        
        // 色をセット
        [m_lineColor[target] setStroke];
        
        // 幅をセット
        m_bezierPath[target].lineWidth = m_lineWidth[target];
        
        // 線を引く
        [m_bezierPath[target] stroke];
        
        // 描画した画像を取得
        UIImage* im = UIGraphicsGetImageFromCurrentImageContext();
        
        // 描画終了
        UIGraphicsEndImageContext();
        
//        [imgView removeFromSuperview];
//        imgView.image = nil;
//        imgView = nil;
//        imgView = [[UIImageView alloc] initWithImage:im];
//        [view addSubview:imgView];
//        [self createThumbnail:imgView :target];
    
        UIImageView* iv = [[UIImageView alloc] initWithImage:im];
        [self createThumbnail:iv: target];
        iv.image = nil;
        iv = nil;
        
        return im;
    }
}

/*!
 * @brief   描画開始の設定を行う
 *
 * @param   point 描画位置
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  YES 正常終了
 * @return  NO 引数エラー
 * @note    公開関数。タッチ開始時にコールする。
 */
- (BOOL)handeraseBegin:(CGPoint)point :(int)target
{
    if (target < 0) {
        return NO;
    }
    // パスの初期化
    m_bezierPathErase[target].lineCapStyle = kCGLineCapRound;
    m_bezierPathErase[target].lineWidth = m_lineWidth[target] + 1;
    m_bezierPath[target].lineWidth = m_lineWidth[target] + 1;
    
    // パスの開始点を設定
    [m_bezierPathErase[target] moveToPoint:point];
    return YES;
}

/*!
 * @brief   viewに描画する
 *
 * @param   view 描画するview
 * @param   imgView 描画画像
 * @param   point 描画位置
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  UIImage
 * @note    公開関数。タッチ時にコールする。
 */
//- (void)eraseLine:(UIView *)view :(UIImageView *)imgView :(CGPoint)point :(int)target
- (UIImage *)eraseLine:(UIView *)view :(CGPoint)point :(int)target
{
    if (target < 0 || view == nil) {
        return nil;
    }
    @autoreleasepool {
        // パスの終了点を追加
        [m_bezierPathErase[target] addLineToPoint:point];
        
        // 非表示の描画領域を生成
        UIGraphicsBeginImageContext(view.frame.size);
        
        // 前回までの手書き表示を再描画
        [m_lineColor[target] setStroke];
        [m_bezierPath[target] stroke];
        
        // 描画領域に前回までに描画した画像を描画する
        //[m_handwriteImg drawAtPoint:CGPointZero];
        
        // 透明の線を手書き表示の上に毎回描画
        [m_bezierPathErase[target] strokeWithBlendMode:kCGBlendModeClear alpha:1.0];
        
        // 描画した画像を取得
        UIImage* im = UIGraphicsGetImageFromCurrentImageContext();
        
        // 描画終了
        UIGraphicsEndImageContext();
        
//        [imgView removeFromSuperview];
//        imgView.image = nil;
//        imgView = nil;
//        imgView = [[UIImageView alloc] initWithImage:im];
//        [view addSubview:imgView];
//        [self createThumbnail:imgView :target];
        
        UIImageView* iv = [[UIImageView alloc] initWithImage:im];
        [self createThumbnail:iv: target];
        iv.image = nil;
        iv = nil;
        
        return im;
    }
}

/*!
 * @brief   ベジェパスの初期化
 *
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  YES 正常終了
 * @return  NO 引数エラー
 * @note    公開関数
 */
- (BOOL)endHandwrite:(int)target
{
    if (target < 0) {
        return NO;
    }
    m_bezierPath[target] = nil;
    m_bezierPathErase[target] = nil;
    return YES;
}

/*!
 * @brief   手書き情報ファイル読み込み
 *
 * @param   path 手書き情報ファイルのパス（ファイル名含む）
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  YES 正常終了
 * @return  NO 引数エラー、読み込み失敗
 * @note    公開関数
 */
- (BOOL)readHandwriteInfoFile:(NSString *)path :(int)target
{
    if (target < 0) {
        return NO;
    }
    // Array初期化
    if ([m_handwriteInfoArray[target] count] != 0) {
        [m_handwriteInfoArray[target] removeAllObjects];
    }
    
    // ファイル全体を読み込む
    NSError* err = nil;
    NSString* allText = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&err];
    if (allText == nil) {
        NSLog(@"%@", err);
        return NO;
    }
    [allText enumerateLinesUsingBlock:^(NSString* line, BOOL* stop) {
        // 1行ずつArrayに格納する
        [self insertHandwriteInfo:line :target];
    }];
    
    return YES;
}

/*!
 * @brief   解放処理
 *
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  YES 正常終了
 * @return  NO 引数エラー
 * @note    公開関数
 */
- (BOOL)finalize:(int)target
{
    if (target < 0) {
        return NO;
    }
    m_handwritePlayTimer[target] = nil;
    m_handwritePlayView[target] = nil;
    m_drawingIndex[target] = 0;
    [m_handwriteInfoArray[target] removeAllObjects];
    //m_drawCount = 0;
    return YES;
}

/*!
 * @brief   手書き情報ファイルからの描画開始
 *
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  YES 正常終了
 * @return  NO 引数エラー
 * @note    公開関数
 */
- (BOOL)startDrawing:(int)target
{
    if (target < 0) {
        return NO;
    }
    // 開始時刻を記録
    m_startPlayTime = [NSDate date];
    
    // タイマーを起動
    [self initTimer:target];
    return YES;
}

/*!
 * @brief   手書き情報ファイルからの描画停止
 *
 * @param   target 対象レイヤ番号（書き込みは0）
 * @return  YES 正常終了
 * @return  NO 引数エラー
 * @note    公開関数
 */
- (BOOL)stopDrawing:(int)target
{
    if (target < 0) {
        return NO;
    }
    // タイマーを止める
    [m_handwritePlayTimer[target] invalidate];
    m_handwritePlayTimer[target] = nil;
    return YES;
}

/*!
 * @brief   手書き表示画像保存しておく
 *
 * @param   iv 手書き表示画像
 * @return  -
 * @note    非公開関数
 */
- (void)createThumbnail:(UIImageView *)iv :(int)target
{
    // 描画の度に更新されるため描画の最後がサムネイルとして記録されることになる
    m_thumbnailData[target] = UIImagePNGRepresentation(iv.image);
}

/*!
 * @brief   手書き表示画像のサムネイル化
 *
 * @param   target 対象レイヤ番号
 * @param   filePath サムネイルを書き込むファイルパス
 * @return  -1 ファイルが存在しない
 * @return  -2 サムネイルデータがない
 * @return  -3 引数エラー
 * @return  0 正常終了
 * @note    公開関数。手書き入力済みもしくは手書き表示済みでなければ取得できない。
 */
- (int)getThumbnail:(UIView *)view :(int)target :(NSString *)filePath
{
    if (target < 0 || view == nil || filePath == nil) {
        return -3;
    }
    NSFileManager* fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:filePath]) {
        // 書き込むファイルが存在しない
        return -1;
    }
    
    if (m_thumbnailData[target] == nil) {
        // サムネイルデータが存在しない
        return -2;
    }
    
    // ファイルへPNGを書き込み
    [m_thumbnailData[target] writeToFile:filePath atomically:YES];
    return 0;
}

/**
 現在の再生時間を取得する
 現状は単純だが、スロー再生、巻き戻し、早送りなんかが入るとややこしくなる可能性大
 */
- (float)getPlayTime
{
    //return m_drawCount * m_timerInterval;
    
    // 現在時刻を取得
    NSDate* now = [NSDate date];
    
    // 再生開始時刻との差分を取得
    float diff = [now timeIntervalSinceDate:m_startPlayTime];
    
    return diff;
}

/**
 タイマーによって呼ばれるコールバック
 */
- (void)drawTimerCallback:(NSTimer *)timer
{
    //m_drawCount++;
    
    // 引数からターゲットを取得
    NSString* str = [[timer userInfo] objectForKey:@"TARGET"];
    int target = [str intValue];
    
    // 現在の再生時間
    float fNowTime = [self getPlayTime];
    if (m_slowPlay == YES) {
        // スロー再生の場合
        NSDate* prev = m_slowStartPlayTime;
        m_slowStartPlayTime = [NSDate date];
        // 現在と前回スロー再生時間の差分を取得
        if (prev != 0) {
            // 現在と前回スロー再生時間の差分を取得。スロー時間を基準として。
            m_slowPlayTime += ([m_slowStartPlayTime timeIntervalSinceDate:prev] * 0.5);
        }
        // スロー再生時間を差し引いてスロー再生された時間を合わせる
        fNowTime -= m_slowPlayTime;
    } else {
        // 通常再生の場合
        // スロー再生時間を差し引いてスロー再生された時間と通常再生された時間を合わせる
        fNowTime -= m_slowPlayTime;
        // スロー再生時間をここで初期化しておく
        m_slowStartPlayTime = 0;
    }
    
    int drawnFrameNum = 0;
    
    for (int i = m_drawingIndex[target]; i < [m_handwriteInfoArray[target] count]; i++) {
        // 次の手書き情報を取得
        HandwriteInfo nextDrawInfo;
        [(NSValue *)[m_handwriteInfoArray[target] objectAtIndex:i] getValue:&nextDrawInfo];
        
        // 現在の再生時間と手書き情報のタイムスタンプを比較
        //float nextTimeStamp = CMTimeGetSeconds(nextDrawInfo.timestamp);
        float nextTimeStamp = nextDrawInfo.timestamp;
        
        if (fNowTime < nextTimeStamp) {
            // まだ達していなければコールバックを終了
            break;
        }
        
        if (nextDrawInfo.type == 0) {
            // 過ぎていれば描画する
            if (nextDrawInfo.erase == 0) {
                if (nextDrawInfo.startFlag == 1) {
                    // パスの開始点を設定
                    [self handwriteBegin:nextDrawInfo.point :target];
                }
                [self setColor:nextDrawInfo.color :target];
                [self setLineWidth:nextDrawInfo.lineSize :target];
                if (nextDrawInfo.object == HANDWRITE_LINE_OBJECT) {
                    //[self drawLine:m_handwritePlayView[target] :m_handwritePlayImgView[target] :nextDrawInfo.point :target];
                    UIImage* im = [self drawLine:m_handwritePlayView[target] :nextDrawInfo.point :target];
                    m_handwritePlayImgView[target].image = nil;
                    [m_handwritePlayImgView[target] removeFromSuperview];
                    m_handwritePlayImgView[target].layer.sublayers = nil;
                    m_handwritePlayImgView[target] = nil;
                    m_handwritePlayImgView[target] = [[UIImageView alloc] initWithImage:im];
                    [m_handwritePlayView[target] addSubview:m_handwritePlayImgView[target]];
                    //TODO:m_handwritePlayViewを配列で確保し、リセット時にnil化する
                } else if (nextDrawInfo.object == HANDWRITE_SQUARE_OBJECT) {
                    //[self drawSquareObject:m_handwritePlayView[target] :m_handwritePlayImgView[target] :nextDrawInfo.point :target];
                    UIImage* im = [self drawSquareObject:m_handwritePlayView[target] :nextDrawInfo.point :target];
                    m_handwritePlayImgView[target].image = nil;
                    [m_handwritePlayImgView[target] removeFromSuperview];
                    m_handwritePlayImgView[target].layer.sublayers = nil;
                    m_handwritePlayImgView[target] = nil;
                    m_handwritePlayImgView[target] = [[UIImageView alloc] initWithImage:im];
                    [m_handwritePlayView[target] addSubview:m_handwritePlayImgView[target]];
                } else if (nextDrawInfo.object == HANDWRITE_CIRCLE_OBJECT) {
                    //[self drawCircleObject:m_handwritePlayView[target] :m_handwritePlayImgView[target] :nextDrawInfo.point :target];
                    UIImage* im = [self drawCircleObject:m_handwritePlayView[target] :nextDrawInfo.point :target];
                    m_handwritePlayImgView[target].image = nil;
                    [m_handwritePlayImgView[target] removeFromSuperview];
                    m_handwritePlayImgView[target].layer.sublayers = nil;
                    m_handwritePlayImgView[target] = nil;
                    m_handwritePlayImgView[target] = [[UIImageView alloc] initWithImage:im];
                    [m_handwritePlayView[target] addSubview:m_handwritePlayImgView[target]];
                }
            } else {
                if (nextDrawInfo.startFlag == 1) {
                    // パスの開始点を設定
                    [self handeraseBegin:nextDrawInfo.point :target];
                }
                
                // TODO:API化
                m_handwritePlayView[target] = [m_instance setHandwritePlayView:target];
                
                //[self eraseLine:m_handwritePlayView[target] :m_handwritePlayImgView[target] :nextDrawInfo.point :target];
                UIImage* im = [self eraseLine:m_handwritePlayView[target] :nextDrawInfo.point :target];
                m_handwritePlayImgView[target].image = nil;
                [m_handwritePlayImgView[target] removeFromSuperview];
                m_handwritePlayImgView[target].layer.sublayers = nil;
                m_handwritePlayImgView[target] = nil;
                m_handwritePlayImgView[target] = [[UIImageView alloc] initWithImage:im];
                [m_handwritePlayView[target] addSubview:m_handwritePlayImgView[target]];
            }
        } else {
            if (nextDrawInfo.play != 1) {
                [m_instance playControl:nextDrawInfo.play :nextDrawInfo.timestampPlayer];
            } else {
                if (m_slowPlay == NO) {
                    [m_instance playControl:nextDrawInfo.play :nextDrawInfo.timestampPlayer];
                } else {
                    [m_instance playControl:HANDWRITE_PLAYER_SLOW_PLAY :nextDrawInfo.timestampPlayer];
                }
            }
        }
        drawnFrameNum++;
        
        // 複数個過ぎている可能性があるので次に行く
        continue;
    }
    
    // 次描画するはずのフレーム番号を記憶
    m_drawingIndex[target] += drawnFrameNum;
}

/**
 タイマー初期化
 */
- (void)initTimer:(int)target
{
    //タイマーが動いていたら一旦止める
    if ([m_handwritePlayTimer[target] isValid]) {
        [m_handwritePlayTimer[target] invalidate];
        m_handwritePlayTimer[target] = nil;
    }
    
    // 現状レイヤターゲットのみしかパラメタは考えていないのでstringで渡す（他にも出てくればNSArray）
    NSString* value = [NSString stringWithFormat:@"%d", target];
    NSDictionary* dic = [NSDictionary dictionaryWithObject:value forKey:@"TARGET"];
    // タイマー起動
    m_handwritePlayTimer[target] = [NSTimer scheduledTimerWithTimeInterval:m_timerInterval target:self selector:@selector(drawTimerCallback:) userInfo:dic repeats:true];
    [m_handwritePlayTimer[target] fire];
}

/**
 描画タイマの処理間隔をセット
 */
- (void)setHandwriteTimerInterval:(float)interval
{
    m_timerInterval = interval;
}

/**
 読み込んだ描画情報をArrayに格納する
 */
- (void)insertHandwriteInfo:(NSString *)strDrawInfo :(int)target
{
    // ”,”区切りで切る
    NSArray* drawInfoArray = [strDrawInfo componentsSeparatedByString:@","];
    
    // 1つ目は種別(char)
    char type = [drawInfoArray[0] intValue];
    
    // 2つ目は入力かまたは削除(char)
    char erase = [drawInfoArray[1] intValue];
    
    // 3つ目は手書き開始(char)
    char start = [drawInfoArray[2] intValue];
    
    // 4つ目はタイムスタンプ(float)
    int object = [drawInfoArray[3] intValue];
    
    // 5つ目はタイムスタンプ(float)
    float ts = [drawInfoArray[4] floatValue];
    
    // 6つ目は開始x座標(float)
    float startX = [drawInfoArray[5] floatValue];
    
    // 7つ目は開始y座標(float)
    float startY = [drawInfoArray[6] floatValue];
    
    // 8つ目は色(char)
    char color = [drawInfoArray[7] intValue];
    
    // 9つ目は線幅(char)
    char lineSize = [drawInfoArray[8] intValue];
    
    // 10つ目は再生制御(char)
    char play = [drawInfoArray[9] intValue];
    
    // 11つ目はタイムスタンプ(float)
    float tsPlayer = [drawInfoArray[10] floatValue];
    
    // 構造体に格納
    CGPoint ptStart = CGPointMake(startX, startY);
    HandwriteInfo df = {type, erase, start, object, ts, ptStart, color, lineSize, play, tsPlayer};
    
    // Arrayに格納
    NSValue* dfValue = [NSValue value:&df withObjCType:@encode(HandwriteInfo)];
    [m_handwriteInfoArray[target] addObject:dfValue];
}

/*!
 * @brief   手書き情報のアップロード
 *
 * @param   textFilePath 手書きテキストのファイルパス
 * @param   thumbnailFilePath 手書きサムネイルのファイルパス
 * @param   play_list_id 対象プレイリストID
 * @param   timestamp 対象プレイリストのタイムスタンプ
 * @param   backendURL リクエストするURL
 * @param   token リクエストに必要なトークン
 * @return  true 正常終了
 * @return  -1 手書き情報がない
 * @return  -2 サムネイルデータがない
 * @return  -3 引数エラー
 * @note    公開関数。
 */
- (int)handwriteInfoUpload:(NSString *)textFilePath :(NSString *)thumbnailFilePath :(NSString *)play_list_id :(double)timestamp :(NSString *)backendURL :(NSString *)token
{
    NSNumber* num;
    NSString* str;
    NSMutableDictionary* mutableDic = [NSMutableDictionary dictionary];
    
    if (textFilePath == nil || thumbnailFilePath == nil || play_list_id == nil || backendURL == nil || token == nil) {
        return -3;
    }
    // プレイリストID
    str = [NSString stringWithString:play_list_id];
    [mutableDic setObject:play_list_id forKey:@"play_list_id"];
    // 1球動画のタイムスタンプ
    num = [[NSNumber alloc] initWithDouble:timestamp];
    [mutableDic setValue:num forKey:@"timestamp"];
    // 手書きテキストの作成UNIX時間
    str = [textFilePath stringByDeletingPathExtension];
    NSRange search = [str rangeOfString:@"_" options:NSBackwardsSearch];
    str = [str substringFromIndex:search.location + 1];
    double t = [str floatValue];
    num = [NSNumber numberWithDouble:t];
    [mutableDic setValue:num forKey:@"create_time"];
    // 手書きテキスト
    // ファイル全体を読み込む
    NSError* err = nil;
    str = [NSString stringWithContentsOfFile:textFilePath encoding:NSUTF8StringEncoding error:&err];
    if (str == nil) {
        NSLog(@"%@", err);
        return -1;
    }
    [mutableDic setObject:str forKey:@"text"];
    // 手書きサムネイル
    NSData* thumb = [NSData dataWithContentsOfFile:thumbnailFilePath];
    if (thumb == nil) {
        return -2;
    }
    str = [thumb base64EncodedStringWithOptions:0]; // base64で書き込む
    [mutableDic setObject:str forKey:@"thumbnail"];
    
    // JSONファイル化
    NSData* data =[NSJSONSerialization dataWithJSONObject:mutableDic options:2 error:&err];
    
    NSURL* url = [NSURL URLWithString:backendURL]; // 通知を行うサーバのURL
    //NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSMutableURLRequest* req = [NSMutableURLRequest requestWithURL:url];
    
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody:data];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // HTTPヘッダにトークンを追加
    [req setValue:token forHTTPHeaderField:@"X-PitchBase-API-Token"];
    // 通信開始
    NSURLConnection* c = [[NSURLConnection alloc] initWithRequest:req delegate:self];

    return true;
}

/*!
 * @brief   手書き情報のダウンロード
 *
 * @param   backendURL リクエストするURL
 * @param   token リクエストに必要なトークン
 * @param   handwrite_id バックエンドで管理している手書き情報のID
 * @return  true 正常終了（ダウンロードが完了している訳ではない）
 * @return  false 引数エラー
 * @note    公開関数。
 */
- (BOOL)handwriteInfoDownload:(NSString *)backendURL :(NSString *)token :(NSString *)handwrite_id
{
    if (backendURL == nil || token == nil) {
        return NO;
    }
    NSURL* url = [NSURL URLWithString:backendURL]; // 通知を行うサーバのURL
    if (handwrite_id != nil) {
        // play_list_idがnilではないので一覧取得ではなく一件取得
        backendURL = [backendURL stringByAppendingString:@"/"];
        backendURL = [backendURL stringByAppendingString:handwrite_id];
        url = [NSURL URLWithString:backendURL];
    }
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSMutableURLRequest* req = [NSMutableURLRequest requestWithURL:url];
    
    [req setHTTPMethod:@"GET"];
    // HTTPヘッダにトークンを追加
    [req setValue:token forHTTPHeaderField:@"X-PitchBase-API-Token"];
    // 通信開始
    NSURLConnection* c = [[NSURLConnection alloc] initWithRequest:req delegate:self];
    
    return YES;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    m_receivedData = nil;
    m_receivedData = [NSMutableData data];
    m_receiveStatus = 0;
    //NSString* str = [[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding];
    [m_receivedData appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //TODO:受信完了を通知するIFを用意するか、m_receiveStatusをポーリングさせるか
    
    // 受信データをJSONデータとして扱う
    NSMutableArray* marray = [NSMutableArray array];
    marray = [NSJSONSerialization JSONObjectWithData:m_receivedData options:NSJSONReadingMutableContainers error:nil];
    NSString* error = [marray valueForKey:@"error"];
    NSString* results = [marray valueForKey:@"results"];
    // errorというキーが存在すれば受信エラー。errorというキーが存在せず、かつresultsというキー存在すれば受信成功。
    // TODO:resultsが来ない場合がある？
    if (error == nil && results != nil) {
        m_receiveStatus = 1;
    } else {
        m_receiveStatus = -1;
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(nonnull NSError *)error
{
    m_receiveStatus = -1;
    // エラー通知する？
    m_receivedData = nil;
    m_receivedData = [NSMutableData data];
}

- (void)URLSession:(NSURLSession *)session dataTask:(nonnull NSURLSessionDataTask *)dataTask didReceiveData:(nonnull NSData *)data
{
    // 必要であればダウンロード受信データ処理
}

/*!
 * @brief   手書き情報の読み込み
 *
 * @param   dir 手書きファイルを格納するディレクトリ
 * @param   textFilePath 読み込み後の手書きファイルパス
 * @param   thumbnailFilePath 読み込み後のサムネイルファイルパス
 * @param   play_list_id 読み込んだ対象のプレイリストID
 * @param   timestamp 読み込んだ対象のタイムスタンプ
 * @param   createTime 読み込んだ対象の手書きファイルが作成された時間
 * @return  0 正常終了
 * @return  -1 手書きファイル受信失敗
 * @return  -2 ディレクトリ作成失敗
 * @return  -3 引数エラー
 * @note    公開関数。ダウンロードした手書き情報を解析して保存。
 */
- (int)handwriteReadInfo:(NSString *)dir :(NSString **)textFilePath :(NSString **)thumbnailFilePath :(NSString **)play_list_id :(double *)timestamp :(double *)createTime
{
    if (dir == nil) {
        return -3;
    }
    // 受信成功していなければ処理しない
    if (m_receiveStatus != 1) {
        return -1;
    }
    // TODO:受信データの整理
    NSMutableArray* marray = [NSMutableArray array];
    marray = [NSJSONSerialization JSONObjectWithData:m_receivedData options:NSJSONReadingMutableContainers error:nil];
    *play_list_id = [marray valueForKeyPath:@"play_list_id"];
    NSNumber* num = [marray valueForKeyPath:@"timestamp"];
    *timestamp = [num doubleValue];
    num = [marray valueForKeyPath:@"create_time"];
    *createTime = [num doubleValue];
    NSString* str = [marray valueForKeyPath:@"text"];
    // TODO:ファイルを作成して手書きテキストを書き込む
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSError* error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dir]) {
        // ディレクトリを作成
        [fileManager createDirectoryAtPath:dir withIntermediateDirectories:YES attributes:nil error:&error];
        if (error != nil) {
            return -2;
        }
    }
    NSString* timestr = [NSString stringWithFormat:@"%f", &createTime];
    
    NSString* name = [NSString stringWithFormat:@"/handwrite_%@_%@.txt", *play_list_id, timestr];
    *textFilePath = [dir stringByAppendingString:name];
    BOOL isExist = [fileManager fileExistsAtPath:*textFilePath];
    if (isExist == YES) {
        // ファイルが存在すれば一旦削除
        [fileManager removeItemAtPath:*textFilePath error:&error];
        [fileManager createFileAtPath:*textFilePath contents:[NSData data] attributes:nil];
    } else if (isExist == NO) {
        [fileManager createFileAtPath:*textFilePath contents:[NSData data] attributes:nil];
    }
    // ファイルへ手書きテキストを書き込み
    NSData* data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSFileHandle* fh = [NSFileHandle fileHandleForWritingAtPath:*textFilePath];
    [fh seekToEndOfFile];
    [fh writeData:data];
    [fh closeFile];
    
    str = [marray valueForKeyPath:@"thumbnail"];
    NSData* thumb = [[NSData alloc] initWithBase64EncodedString:str options:0];
    // TODO:ファイルを作成してサムネイルを書き込む
    // 手書きサムネイルファイル作成
    name = [NSString stringWithFormat:@"/handwrite_thumbnail_%@_%@.png", *play_list_id, timestr];
    *thumbnailFilePath = [dir stringByAppendingString:name];
    isExist = [fileManager fileExistsAtPath:*thumbnailFilePath];
    if (isExist == YES) {
        // ファイルが存在すれば一旦削除
        [fileManager removeItemAtPath:*thumbnailFilePath error:&error];
        [fileManager createFileAtPath:*thumbnailFilePath contents:[NSData data] attributes:nil];
    } else if (isExist == NO) {
        [fileManager createFileAtPath:*thumbnailFilePath contents:[NSData data] attributes:nil];
    }
    // ファイルへサムネイルを書き込み
    fh = [NSFileHandle fileHandleForWritingAtPath:*thumbnailFilePath];
    [fh seekToEndOfFile];
    [fh writeData:thumb];
    [fh closeFile];
    
    // iCloudへのバックアップをしない
    NSURL* url = [NSURL fileURLWithPath:*textFilePath];
    BOOL success = [self addSkipBackupAttributeToItem:url];
    url = [NSURL fileURLWithPath:*thumbnailFilePath];
    success = [self addSkipBackupAttributeToItem:url];
    
    return 0;
}

// iCloudへのバックアップをさせない
- (BOOL)addSkipBackupAttributeToItem:(NSURL *)URL
{
    const char* filePath = [[URL path] fileSystemRepresentation];
    
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    if (result == 0) {
        return YES;
    } else {
        return NO;
    }
}

/*!
 * @brief   手書きに関連するファイルの削除
 *
 * @param   extensions 削除するファイルの拡張子
 * @return  YES 正常終了
 * @return  NO ディレクトリ検索失敗
 * @note    非公開関数。手書き入力済みもしくは手書き表示済みでなければ取得できない。
 */
- (BOOL)cleanHandwriteWithExtension:(NSArray *)extensions
{
    //NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    //NSString* libraryDirectory = [paths objectAtIndex:0];
    //NSString* _handwriteCacheDirectory = [libraryDirectory stringByAppendingString:@"/handwrite"];
    
    NSString* path = [paths objectAtIndex:0];
    NSString* handwritePath = [path stringByAppendingString:@"/handwrite"];
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSError* error;
    NSArray* allHandwriteFileName = [fileManager contentsOfDirectoryAtPath:handwritePath error:&error];
    if (error){
        return NO;
    }
    
    NSString* rmPath;
    for (NSString* content in allHandwriteFileName) {
        //if ([extension isEqualToString:[content pathExtension]]) {
        if ([extensions containsObject:[content pathExtension]]) {
            rmPath = [handwritePath stringByAppendingPathComponent:content];
            // TODO:レイヤ重ね試験用に消さないようにしておくstart
            if ([content isEqualToString:@"handwrite_test_20160729.txt"] != YES && [content isEqualToString:@"handwrite_test2_20160729.txt"] != YES) {
                [fileManager removeItemAtPath:rmPath error:&error];
            }
            // TODO:レイヤ重ね試験用に消さないようにしておくend
        }
    }
    return YES;
}
@end
