//
//  Handwrite.h
//  SportsVideoPF_LibHandwrite
//  All Rights Reserved, Copyright(c) FUJITSU LIMITED 2016
//

#pragma once
#import <sys/xattr.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "HandwriteProtocol.h"

#define HANDWRITE_MAX_LAYER_NUM 2 // 手書き重ね表示最大数
#define HANDWRITE_LINE_OBJECT 0
#define HANDWRITE_SQUARE_OBJECT 1
#define HANDWRITE_CIRCLE_OBJECT 2
#define HANDWRITE_PLAYER_PLAY 1
#define HANDWRITE_PLAYER_PAUSE 2
#define HANDWRITE_PLAYER_SEEK 3
#define HANDWRITE_PLAYER_SLOW_PLAY 4

/**
 手書き情報構造体
 */
typedef struct {
    int type; // 手書き情報かプレーヤ情報か
    int erase; // 手書き入力か手書き削除か
    int startFlag; // 手書き情報の場合、描画開始かどうか
    int object; // 線、四角、円
    float timestamp; // 手書き入力からの時間
    CGPoint point; // 手書き情報の場合、手書き位置
    //CGPoint pointEnd; // 手書き情報の場合、手書き完了位置
    int color; // 手書き入力サイズ
    int lineSize; // 手書き入力色
    int play; // プレーヤ情報の場合、プレーヤが再生か停止か
    float timestampPlayer; // プレーヤ情報の場合、プレーヤのタイムスタンプ
} HandwriteInfo;

/**
 手書き表示用Viewの描画を行うクラス。
 手書き情報ファイルを読み込み、タイマを利用して適切な時刻に描画を行う。
 動画側のAVPlayerとは現状同期せず、独立して動く。
 */
@interface HandwriteLib :UIViewController <HandwriteProtocol>
{
    id<ComparisonViewProtocol> m_instance; // ComparisonViewControllerのインスタンス
    
    BOOL m_handwriteMode; // 手書き入力モードか手書き表示モードかを示す
    UIBezierPath* m_bezierPath[HANDWRITE_MAX_LAYER_NUM]; // 手書き曲線パス
    UIBezierPath* m_bezierPathErase[HANDWRITE_MAX_LAYER_NUM]; // 手書き曲線パス
    UIImage* m_handwriteImg; // 最後の手書き画像を保持
    
    UIView* m_handwritePlayView[HANDWRITE_MAX_LAYER_NUM]; // 手書き表示用ビュー
    UIImageView* m_handwritePlayImgView[HANDWRITE_MAX_LAYER_NUM]; // 手書き表示用画像
    
    NSTimer* m_handwritePlayTimer[HANDWRITE_MAX_LAYER_NUM]; // 手書き表示用タイマー
    float m_timerInterval; // 手書き表示インターバル
    NSMutableArray* m_handwriteInfoArray[HANDWRITE_MAX_LAYER_NUM]; // 手書き情報構造体配列
    //int m_drawCount;
    int m_drawingIndex[HANDWRITE_MAX_LAYER_NUM]; // 次回手書き表示フレーム番号
    NSDate* m_startPlayTime; // 手書き表示された時間
    NSDate* m_slowStartPlayTime; // スロー再生開始された時間
    float m_slowPlayTime; // スロー再生された時間
    float m_lineWidth[HANDWRITE_MAX_LAYER_NUM]; // 手書き線の太さ
    UIColor* m_lineColor[HANDWRITE_MAX_LAYER_NUM]; // 手書き線の色
    NSData* m_thumbnailData[HANDWRITE_MAX_LAYER_NUM];
    BOOL m_slowPlay; // スロー再生かどうか
    NSMutableData* m_receivedData; // 手書き情報受信データ
    int m_receiveStatus; // 手書き受信状態 0:受信なし 1:受信成功 -1:受信エラー
}
@end
